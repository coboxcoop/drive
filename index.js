const logger = require('@coboxcoop/logger')
const { KappaDrive } = require('kappa-drive')
const mount = require('kappa-drive-mount')
const assert = require('assert')
const maybe = require('call-me-maybe')
const { keyIds } = require('@coboxcoop/constants')
const crypto = require('@coboxcoop/crypto')
const debug = logger('@coboxcoop/drive')
const { assign } = Object

const { metadata: metadataId, content: contentId } = keyIds

class Drive extends KappaDrive {
  /**
   * inherit kappa-drive file system operations
   * @constructor
   */
  constructor (opts = {}) {
    super(opts.corestore, opts.address, {
      logger: logger('kappa-drive'),
      metadataId,
      contentId,
      ...opts
    })

    this._id = opts._id || crypto.randomBytes(2).toString('hex')
    this.location = null
    this._unmount = null
  }

  ready (callback) {
    return maybe(callback, new Promise((resolve, reject) => {
      super.ready((err) => {
        if (err) return reject(err)
        return resolve()
      })
    }))
  }

  async mount (opts = {}) {
    assert(opts.location, 'provide a mount location')
    if (this._unmount) return false // already mounted
    this._unmount = await mount(this, opts.location, assign({ logger: logger('kappa-drive-mount')}, opts))
    this.location = opts.location
    return this.location
  }

  async unmount () {
    if (!this._unmount) return false // already unmounted
    await this._unmount()
    this._unmount = null
    var location = this.location
    this.location = null
    return location
  }

  async ls (subdir = '/') {
    assert(typeof subdir === 'string', 'path must be a string')

    const files = await new Promise((resolve, reject) => {
      this.readdir(subdir, (err, files) => {
        if (err) reject(err)
        resolve(files)
      })
    })

    const stats = await Promise.all(files.map((file) => {
      return new Promise((resolve, reject) => {
        this.lstat(file, (err, statObj) => {
          if (err) reject(err)
          resolve(statObj)
        })
      })
    }))

    return stats.reduce((acc, cur, i) => {
      acc[files[i]] = cur
      return acc
    }, {})
  }

  size () {
    return new Promise((resolve, reject) => {
      var size = Array
        .from(this._drives.values())
        .reduce(sumDriveSpace, 0)

      return resolve(size)

      function sumDriveSpace (acc, drive) {
        const contentCores = drive._contentStates.cache.keys()
        for (const core in contentCores) {
          acc += core.byteLength || 0
        }
        acc += drive.metadata.byteLength || 0
        return acc
      }
    })
  }

  isMounted () {
    return Boolean(this._unmount)
  }

  _gracefulUnmount (cb) {
    if (!this.isMounted()) return cb()

    this.unmount()
      .catch((err) => {
        if (err.code === 'ERR_ASSERTION') return cb()
        debug({ id: this._id, msg: 'failed to unmount drive', err })
        return cb(err)
      })
      .then(() => cb())
  }
}

module.exports = (...args) => new Drive(...args)
module.exports.Drive = Drive
