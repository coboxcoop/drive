const { describe } = require('tape-plus')
const crypto = require('@coboxcoop/crypto')
const Corestore = require('corestore')
const RAM = require('random-access-memory')
const sinon = require('sinon')
const proxyquire = require('proxyquire')
const Multifeed = require('@coboxcoop/multifeed')
const Kappa = require('kappa-core')
const { KappaDrive } = require('kappa-drive')
const memdb = require('level-mem')

const Drive = require('../')

describe('@coboxcoop/space: Drive', (context) => {
  context('constructor()', (assert, next) => {
    var corestore = new Corestore(RAM)

    var handler = Drive({
      corestore,
      db: memdb(),
      address: crypto.address().toString('hex'),
      feeds: new Multifeed(corestore),
      core: new Kappa(),
      deriveKeyPair: crypto.keyPair.bind(null, crypto.masterKey())
    })

    assert.ok(handler instanceof KappaDrive, 'extends KappaDrive')
    next()
  })

  context('ls()', (assert, next) => {
    var corestore = new Corestore(RAM)

    var handler = Drive({
      corestore,
      db: memdb(),
      address: crypto.address().toString('hex'),
      feeds: new Multifeed(corestore),
      core: new Kappa(),
      deriveKeyPair: crypto.keyPair.bind(null, crypto.masterKey())
    })

    handler.ready(() => {
      var filename = 'hello.txt'

      handler.writeFile(filename, 'world', async (err) => {
        assert.error(err, 'no error')

        var dir = await handler.ls()
        assert.equal(Object.keys(dir)[0], filename, 'lists file')
        assert.true(dir[filename].mtime > 10000, 'file has a timestamp')
        next()
      })
    })
  })

  context('mount()', async (assert, next) => {
    var corestore = new Corestore(RAM)
    var stub = sinon.stub().resolves(new Promise((resolve, reject) => resolve()))
    var ProxyHandler = proxyquire('../', { 'kappa-drive-mount': stub })

    var handler = ProxyHandler({
      corestore,
      db: memdb(),
      address: crypto.address().toString('hex'),
      feeds: new Multifeed(corestore),
      core: new Kappa(),
      deriveKeyPair: crypto.keyPair.bind(null, crypto.masterKey())
    })

    await handler.ready()

    try {
      await handler.mount()
    } catch (err) {
      assert.ok(err, 'throws an error')
      assert.same(err.message, 'provide a mount location', 'location required')
    }

    var opts = { location: './mnt' }
    var location = await handler.mount(opts)
    assert.same(location, './mnt', 'returns location')
    sinon.assert.calledOnce(stub)

    sinon.restore()
    next()
  })

  context('unmount()', async (assert, next) => {
    var corestore = new Corestore(RAM)

    var handler = Drive({
      corestore,
      db: memdb(),
      address: crypto.address().toString('hex'),
      feeds: new Multifeed(corestore),
      core: new Kappa(),
      deriveKeyPair: crypto.keyPair.bind(null, crypto.masterKey())
    })

    await handler.ready()

    try {
      await handler.unmount()
    } catch (err) {
      assert.ok(err, 'throws an error')
      assert.same(err.message, 'not mounted', 'not yet mounted')
    }

    handler.location = './mnt'
    handler._unmount = async () => {}
    var location = await handler.unmount()
    assert.same(location, './mnt', 'returns location')

    next()
  })
})
