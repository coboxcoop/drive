# drive

<div align="center">
  <img src="https://cobox.cloud/src/svg/logo.svg">
</div>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Table of Contents

  - [About](#about)
  - [Install](#install)
  - [Usage](#usage)
  - [API](#api)
  - [Contributing](#contributing)
  - [License](#license)

## About
**CoBox** is an encrypted p2p file system and distributed back-up tool. [README](https://gitlab.com/coboxcoop/readme) provides a map of the project.

`drive` wraps `KappaDrive` with some async functions and handlers for easy use in the API.

## Install
```
npm install @coboxcoop/drive
```

## Usage

See tests for usage.

## API

`const Drive = require('@coboxcoop/drive')`

`var drive = Drive(opts)`

Returns a drive object. All opts are passed down to kappa-drive.

### `drive.ready(callback)`

callback is called when the drive is ready to use.

### `drive.mount`

mounts a kappa-drive to a file system location using fuse-native

### `drive.unmount`

unmounts the drive

### `drive.isMounted`

boolean for whether drive is currently mounted

### `drive.ls`

returns a list of file stat objects

### `drive.size`

outputs the total bytes used by all hyperdrives that compose the kappa-drive

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

# License

[`AGPL-3.0-or-later`](./LICENSE)
